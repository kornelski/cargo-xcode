//! cargo-xcode is meant to be used from command line. See [CLI usage docs](https://lib.rs/cargo-xcode).

use cargo_metadata::Package;
use crc::{Crc, CRC_64_ECMA_182};
use std::borrow::Cow;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::{fs, io};

mod plist;

struct XcodeTarget {
    name_label: String,
    cargo_file_name: String,
    xcode_product_name: String,
    xcode_file_name: String,
    compiler_flags: String,
    file_type: &'static str,
    prod_type: &'static str,
    supported_platforms: Cow<'static, str>,
    skip_install: bool,
}

struct XcodeTargetIds<'a> {
    target: &'a XcodeTarget,
    id: String,
    compile_cargo_id: String,
    manifest_path_build_object_id: String,
    prod_conf_debug_id: String,
    prod_conf_list_id: String,
    prod_conf_release_id: String,
    prod_id: String,
}

pub struct Generator {
    crc: Crc<u64>,
    id_base: u64,
    package: Package,
    output_dir: PathBuf,
    custom_project_name: Option<String>,
    skip_install_everything: bool,
    use_rustup_nightly: bool,
    default_features: bool,
    features: String,
    allowed_platforms: Option<Vec<String>>,
    xcconfigs: Vec<String>,
}

struct XcodeTargetKindProps {
    name_label: String,
    cargo_file_name: String,
    xcode_file_name: String,
    xcode_product_name: String,
    file_type: &'static str,
    prod_type: &'static str,
    supported_platforms: Cow<'static, str>,
    skip_install: bool,
}

const STATIC_LIB_APPLE_PRODUCT_TYPE: &str = "com.apple.product-type.library.static";
const DY_LIB_APPLE_PRODUCT_TYPE: &str = "com.apple.product-type.library.dynamic";
const EXECUTABLE_APPLE_PRODUCT_TYPE: &str = "com.apple.product-type.tool";

impl Generator {
    pub fn new(package: Package, output_dir: Option<&Path>, custom_project_name: Option<String>) -> io::Result<Self> {
        let crc = Crc::<u64>::new(&CRC_64_ECMA_182);
        let id_base = crc.checksum(package.name.as_bytes());

        let output_dir = fs::canonicalize(match output_dir {
            Some(o) => o,
            None => package.manifest_path.as_std_path().parent().ok_or(io::ErrorKind::InvalidInput)?,
        })?;

        Ok(Self {
            crc, id_base, package, output_dir, custom_project_name,
            skip_install_everything: false,
            use_rustup_nightly: false,
            default_features: true,
            features: String::new(),
            allowed_platforms: None,
            xcconfigs: Vec::new(),
        })
    }

    pub fn skip_install(&mut self, in_binaries_too: bool) {
        self.skip_install_everything = in_binaries_too;
    }

    pub fn nightly(&mut self, use_rustup_nightly: bool) {
        self.use_rustup_nightly = use_rustup_nightly;
    }

    pub fn xcconfigs(&mut self, xcconfigs: Vec<String>) {
        if xcconfigs.len() > 2 {
            panic!("Specify maximum of 2 xcconfigs");
        }
        self.xcconfigs = xcconfigs;
    }

    pub fn platforms(&mut self, allowed_platforms: Option<Vec<String>>) {
        self.allowed_platforms = allowed_platforms;
    }

    pub fn features(&mut self, features: &str, default_features: bool) {
        self.features = features.into();
        self.default_features = default_features;
    }

    fn make_id(&self, sort: u8, kind: &str, name: &str) -> String {
        let mut crc = self.crc.digest();
        crc.update(&self.id_base.to_ne_bytes());
        crc.update(kind.as_bytes());
        let kind = crc.finalize();

        let name = self.crc.checksum(name.as_bytes());
        let mut out = format!("CA{:02X}{:08X}{:012X}", sort, kind as u32, name);
        out.truncate(24);
        out
    }

    pub fn write_pbxproj(&self) -> Result<PathBuf, io::Error> {
        let proj_path = self.prepare_project_path()?;
        let proj_data = self.pbxproj()?;

        let pbx_path = proj_path.join("project.pbxproj");
        let mut f = fs::File::create(pbx_path)?;
        f.write_all(proj_data.as_bytes())?;

        Ok(proj_path)
    }

    fn project_targets(&self) -> Vec<XcodeTarget> {
        self.package.targets.iter().flat_map(move |target| {
            let base_name = self.custom_project_name.as_ref().unwrap_or(&target.name).clone();
            let required_features = target.required_features.join(",");
            let dylib_exists = target.kind.iter().any(|k| k == "cdylib");
            let static_suffix = if dylib_exists { "_static" } else { "" };
            target.kind.iter().filter_map(move |kind| {
                let p = match kind.as_str() {
                    "bin" => XcodeTargetKindProps {
                        name_label: format!("{} ({})", target.name, if self.skip_install_everything { "bundled binary" } else { "standalone executable" }),
                        cargo_file_name: target.name.clone(),
                        xcode_file_name: base_name.clone(),
                        xcode_product_name: base_name.clone(),
                        file_type: "compiled.mach-o.executable",
                        prod_type: EXECUTABLE_APPLE_PRODUCT_TYPE,
                        skip_install: self.skip_install_everything,
                        supported_platforms: self.only_allowed_platforms("macosx"),
                    },
                    "cdylib" => XcodeTargetKindProps {
                        name_label: format!("{}.dylib (cdylib)", target.name),
                        cargo_file_name: format!("lib{}.dylib", target.name.replace('-', "_")),
                        xcode_file_name: format!("{base_name}.dylib"),
                        xcode_product_name: base_name.clone(),
                        file_type: "compiled.mach-o.dylib",
                        prod_type: DY_LIB_APPLE_PRODUCT_TYPE,
                        skip_install: self.skip_install_everything,
                        supported_platforms: self.only_allowed_platforms("macosx iphonesimulator iphoneos"),
                    },
                    "staticlib" => XcodeTargetKindProps {
                        name_label: format!("{}.a (static library)", target.name),
                        // must have _static suffix to avoid build errors when dylib also exists
                        cargo_file_name: format!("lib{}.a", target.name.replace('-', "_")),
                        xcode_file_name: format!("lib{base_name}{static_suffix}.a"),
                        xcode_product_name: format!("{base_name}{static_suffix}"),
                        file_type: "archive.ar",
                        prod_type: STATIC_LIB_APPLE_PRODUCT_TYPE,
                        skip_install: true,
                        supported_platforms: self.only_allowed_platforms("xrsimulator xros watchsimulator watchos macosx iphonesimulator iphoneos driverkit appletvsimulator appletvos"),
                    },
                    _ => return None,
                };

                let mut compiler_flags = if p.prod_type == EXECUTABLE_APPLE_PRODUCT_TYPE { format!("--bin '{base_name}'") } else { "--lib".into() };
                if p.prod_type == EXECUTABLE_APPLE_PRODUCT_TYPE && !required_features.is_empty() {
                    use std::fmt::Write;
                    write!(&mut compiler_flags, " --features '{required_features}'").unwrap(); // Xcode escapes \=
                }
                if !self.default_features {
                    compiler_flags += " --no-default-features";
                }

                Some(XcodeTarget {
                    name_label: p.name_label,
                    compiler_flags,
                    supported_platforms: p.supported_platforms,
                    cargo_file_name: p.cargo_file_name,
                    xcode_file_name: p.xcode_file_name,
                    xcode_product_name: p.xcode_product_name,
                    file_type: p.file_type,
                    prod_type: p.prod_type,
                    skip_install: p.skip_install,
                })
            })
        })
        .collect()
    }

    fn target_ids<'a>(&self, cargo_targets: &'a [XcodeTarget]) -> Vec<XcodeTargetIds<'a>> {
        cargo_targets.iter().enumerate().map(|(n, target)| {
            let sort = n as u8;
            let prod_id = self.make_id(sort, target.file_type, &target.cargo_file_name);
            XcodeTargetIds {
                target,
                id: self.make_id(sort, target.file_type, &prod_id),
                compile_cargo_id: self.make_id(sort, "<cargo>", &prod_id),
                manifest_path_build_object_id: self.make_id(sort, "<cargo-toml>", &prod_id),
                prod_conf_debug_id: self.make_id(sort, "<config-debug>", &prod_id),
                prod_conf_list_id: self.make_id(sort, "<config-list>", &prod_id),
                prod_conf_release_id: self.make_id(sort, "<config-release>", &prod_id),
                prod_id,
            }
        }).collect()
    }

    pub fn pbxproj(&self) -> Result<String, io::Error> {
        let main_group_id = self.make_id(0xF0, "", "<root>");
        let prod_group_id = self.make_id(0xF1, "", "Products");
        let frameworks_group_id = self.make_id(0xF2, "", "Frameworks"); // This is a magic name that Xcode uses to show Products
        let project_id = self.make_id(0xF3, "", "<project>");
        let build_rule_id = self.make_id(0xF4, "", "BuildRule");
        let conf_list_id = self.make_id(0xF6, "", "<configuration-list>");
        let conf_release_id = self.make_id(0xF7, "configuration", "Release");
        let conf_debug_id = self.make_id(0xF8, "configuration", "Debug");
        let manifest_path_id = self.make_id(0xF9, "", "Cargo.toml");

        let project_name = self.custom_project_name.as_deref().unwrap_or(&self.package.name);
        let rust_targets = self.project_targets();
        let targets = self.target_ids(&rust_targets);

        let mut main_folder_refs = Vec::new();

        main_folder_refs.push(format!("{manifest_path_id} /* Cargo.toml */"));

        let cargo_toml_path = self.relative_path(self.package.manifest_path.as_std_path())?;

        let xcconfigs = self.xcconfigs.iter().enumerate().map(|(n, xc)| {
            let path = self.relative_path(xc.as_ref())?;
            let id = self.make_id(0xFD + n as u8, "configuration", xc);
            Ok((path, id))
        }).collect::<io::Result<Vec<_>>>()?;

        main_folder_refs.push(format!("{prod_group_id} /* Products */"));
        main_folder_refs.push(format!("{frameworks_group_id} /* Frameworks */"));

        for (path, id) in &xcconfigs {
            main_folder_refs.push(format!("{id} /* {name} */", name = path.file_name().unwrap().to_str().unwrap()));
        }

        let main_folder_refs = main_folder_refs.iter().map(|id| format!("\t\t\t\t{id},\n")).collect::<String>();

        let build_script = include_str!("xcodebuild.sh").replace("  ", " ");

        let supported_platforms_prefixes = rust_targets.iter().flat_map(|t| t.supported_platforms.split(' '))
            .filter_map(|p| p.as_bytes().first())
            .fold(vec![], |mut v, prefix| { if !v.contains(prefix) { v.push(*prefix); } v });

        let common_build_settings = |o: &mut plist::Object<'_>, mode: &str| {
            // build scripts need the macosx toolchain
            if supported_platforms_prefixes.contains(&b'i') { o.kvq("\"ADDITIONAL_SDKS[sdk=i*]\"", "macosx"); }
            if supported_platforms_prefixes.contains(&b'w') { o.kvq("\"ADDITIONAL_SDKS[sdk=w*]\"", "macosx"); }
            if supported_platforms_prefixes.contains(&b'x') { o.kvq("\"ADDITIONAL_SDKS[sdk=x*]\"", "macosx"); }
            if supported_platforms_prefixes.contains(&b'a') { o.kvq("\"ADDITIONAL_SDKS[sdk=a*]\"", "macosx"); }
            o.kv("ALWAYS_SEARCH_USER_PATHS", "NO")
            .kvq("CARGO_TARGET_DIR", "$(PROJECT_TEMP_DIR)/cargo_target")
            .kv("CARGO_XCODE_BUILD_PROFILE", mode)
            .kvq("CARGO_XCODE_FEATURES", &self.features)
            .kvq("CURRENT_PROJECT_VERSION", format_args!("{major}.{minor}", major = self.package.version.major, minor = self.package.version.minor))
            .kv("ENABLE_USER_SCRIPT_SANDBOXING", "NO") // Cargo touches Cargo.lock
            .kvq("MARKETING_VERSION", &self.package.version);
            if mode == "debug" {
                o.kv("ONLY_ACTIVE_ARCH", "YES");
            }
            o.kvq("PRODUCT_NAME", &self.package.name)
            .kvq("RUSTUP_TOOLCHAIN", if self.use_rustup_nightly { "nightly" } else { "" })
            .kv("SDKROOT", "macosx");
            if self.skip_install_everything {
                o.kv("SKIP_INSTALL", "YES");
            }
            o.kv("SUPPORTS_MACCATALYST", "YES");
        };

        let crate_version = env!("CARGO_PKG_VERSION");

        let mut p = plist::Plist::new();
        p.root()
            .comment(format_args!("generated with cargo-xcode {crate_version}"))
            .kv("archiveVersion", "1")
            .obj("classes", None, |_| {})
            .kv("objectVersion", "53")
            .obj("objects", None, |o| {
                o.nl()
                 .section_comment("Begin PBXBuildFile section");
                 for t in &targets {
                    o.kv(
                        format_args!("{} /* Cargo.toml in Sources */", t.manifest_path_build_object_id),
                        format_args!("{{isa = PBXBuildFile; fileRef = {manifest_path_id} /* Cargo.toml */; settings = {{COMPILER_FLAGS = \"{compiler_flags}\"; }}; }}", compiler_flags = t.target.compiler_flags)
                    );
                 }
                 o.section_comment("End PBXBuildFile section")
                 .nl()
                 .section_comment("Begin PBXBuildRule section")
                 .obj(&build_rule_id, Some("PBXBuildRule"), |p| {
                    p.kv("isa", "PBXBuildRule")
                    .kv("compilerSpec", "com.apple.compilers.proxy.script")
                    .kvq("dependencyFile", "$(DERIVED_FILE_DIR)/$(ARCHS)-$(EXECUTABLE_NAME).d")
                    .kvq("filePatterns", "*/Cargo.toml") /* must contain asterisk */
                    .kv("fileType", "pattern.proxy")
                    .array("inputFiles", None, |_| {})
                    .kv("isEditable", "0")
                    .kvq("name", "Cargo project build")
                    .array("outputFiles", None, |a| {
                        a.q("$(TARGET_BUILD_DIR)/$(EXECUTABLE_NAME)");
                    })
                    .kv("runOncePerArchitecture", "0")
                    .kvq("script", format_args!("# generated with cargo-xcode {crate_version}\n{build_script}"));
                 })
                 .section_comment("End PBXBuildRule section")
                 .nl()
                 .section_comment("Begin PBXFileReference section");
                 for t in &targets {
                    // path of product does not seem to work. Xcode writes it, but can't read it.
                    o.kv(
                        format_args!("{prod_id} /* {xcode_file_name} */", prod_id = t.prod_id, xcode_file_name = t.target.xcode_file_name),
                        format_args!("{{isa = PBXFileReference; explicitFileType = {file_type}; includeInIndex = 0; path = {xcode_file_name_q}; sourceTree = BUILT_PRODUCTS_DIR; }}",
                            xcode_file_name_q = plist::quote(&t.target.xcode_file_name),
                            file_type = plist::quote(t.target.file_type),
                        )
                    );
                 }
                 for (xc_path, xc_id) in &xcconfigs {
                    let xc_filename = xc_path.file_name().unwrap().to_str().unwrap();
                    o.kv(
                        format_args!("{xc_id} /* {xc_filename} */"),
                        format_args!("{{isa = PBXFileReference; lastKnownFileType = text; name = {xc_filename_q}; path = {xc_path_q}; sourceTree = \"<group>\"; }}",
                            xc_filename_q = plist::quote(xc_filename), xc_path_q = plist::quote(xc_path.to_str().unwrap())),
                    );
                 }
                 o.kv(
                    format_args!("{manifest_path_id} /* {cargo_toml_path} */", cargo_toml_path = cargo_toml_path.to_str().unwrap()),
                    format_args!("{{isa = PBXFileReference; fileEncoding = 4; lastKnownFileType = text; name = Cargo.toml; path = {cargo_toml_path_q}; sourceTree = \"<group>\"; }}",
                        cargo_toml_path_q = plist::quote(cargo_toml_path.to_str().unwrap()),
                    )
                 )
                 .section_comment("End PBXFileReference section")
                 .nl()
                 .section_comment("Begin PBXGroup section")
                 .obj(&main_group_id, None, |f| {
                    f.kv("isa", "PBXGroup")
                    .array("children", None, |c| {
                        c.raw(main_folder_refs);
                    })
                    .kvq("sourceTree", "<group>");
                 })
                 .obj(&prod_group_id, Some("Products"), |f| {
                    f.kv("isa", "PBXGroup")
                    .array("children", None, |c| {
                        for t in &targets {
                            c.v(format_args!("{} /* {} */", t.prod_id, t.target.xcode_file_name));
                        }
                    })
                    .kv("name", "Products")
                    .kvq("sourceTree", "<group>");
                 })
                 .obj(&frameworks_group_id, Some("Frameworks"), |f| {
                    f.kv("isa", "PBXGroup")
                    .array("children", None, |_| {})
                    .kv("name", "Frameworks")
                    .kvq("sourceTree", "<group>");
                 })
                 .section_comment("End PBXGroup section")
                 .nl()
                 .section_comment("Begin PBXNativeTarget section");
                 for t in &targets {
                    o.obj(&t.id, Some(&t.target.name_label), |d| {
                        d.kv("isa", "PBXNativeTarget")
                        .kv("buildConfigurationList", format_args!("{conf_list_id} /* Build configuration list for PBXNativeTarget \"{name}\" */",
                            conf_list_id = t.prod_conf_list_id, name = t.target.name_label))
                        .array("buildPhases", None, |a| {
                            a.v(format_args!("{} /* Sources */", t.compile_cargo_id));
                        })
                        .array("buildRules", None, |r| {
                            r.v(format_args!("{build_rule_id} /* PBXBuildRule */"));
                        })
                        .array("dependencies", None, |_| {
                        })
                        .kvq("name", &t.target.name_label)
                        .kvq("productName", &t.target.xcode_file_name)
                        .kv("productReference", format_args!("{} /* {} */", t.prod_id, t.target.xcode_file_name))
                        .kvq("productType", t.target.prod_type)
                        ;
                    });
                 }
                 o.section_comment("End PBXNativeTarget section")
                 .nl()
                 .section_comment("Begin PBXProject section")
                 .obj(&project_id, Some("Project object"), |p| {
                    p.kv("isa", "PBXProject")
                    .obj("attributes", None, |a| {
                        a.kv("BuildIndependentTargetsInParallel", "YES")
                        .kv("LastUpgradeCheck", "1510")
                        .obj("TargetAttributes", None, |t| {
                            for o in &targets {
                                t.obj(&o.id, None, |a| {
                                    a.kv("CreatedOnToolsVersion", "9.2")
                                    .kv("ProvisioningStyle", "Automatic");
                                });
                            }
                        });
                    })
                    .kv("buildConfigurationList", format_args!("{conf_list_id} /* Build configuration list for PBXProject \"{project_name}\" */"))
                    .kvq("compatibilityVersion", "Xcode 11.4")
                    .kv("developmentRegion", "en")
                    .kv("hasScannedForEncodings", "0")
                    .array("knownRegions", None, |r| {
                        r.v("en").v("Base");
                    })
                    .kv("mainGroup", main_group_id)
                    .kv("productRefGroup", format_args!("{prod_group_id} /* Products */"))
                    .kvq("projectDirPath", "")
                    .kvq("projectRoot", "")
                    .array("targets", None, |a| {
                        for t in &targets {
                            a.v(format_args!("{id} /* {name} */", id = t.id, name = t.target.name_label));
                        }
                    });
                 })
                 .section_comment("End PBXProject section")
                 .nl()
                 .section_comment("Begin PBXSourcesBuildPhase section");
                 for t in &targets {
                    o.obj(&t.compile_cargo_id, Some("Sources"), |b| {
                        b.kv("isa", "PBXSourcesBuildPhase")
                        .kv("buildActionMask", "2147483647")
                        .array("files", None, |f| {
                            f.v(format_args!("{} /* Cargo.toml in Sources */", t.manifest_path_build_object_id));
                        })
                        .kv("runOnlyForDeploymentPostprocessing", "0");
                    });
                 }
                 o.section_comment("End PBXSourcesBuildPhase section")
                 .nl()
                 .section_comment("Begin XCBuildConfiguration section");
                 for t in &targets {
                    let skip_install_config = t.target.skip_install && !self.skip_install_everything;
                    for [conf_name, conf_id] in [["Release", &t.prod_conf_release_id], ["Debug", &t.prod_conf_debug_id]] {
                        o.obj(conf_id, Some(conf_name), |c| {
                            c.kv("isa", "XCBuildConfiguration")
                            .obj("buildSettings", None, |b| {
                                b.kvq("CARGO_XCODE_CARGO_DEP_FILE_NAME", Path::new(&t.target.cargo_file_name).with_extension("d").file_name().unwrap().to_str().unwrap())
                                .kvq("CARGO_XCODE_CARGO_FILE_NAME", &t.target.cargo_file_name);
                                if skip_install_config {
                                    // Xcode tries to chmod it when archiving, even though it doesn't belong to the archive
                                    b.kvq("INSTALL_GROUP", "")
                                    .kvq("INSTALL_MODE_FLAG", "")
                                    .kvq("INSTALL_OWNER", "");
                                }
                                 if t.target.prod_type == DY_LIB_APPLE_PRODUCT_TYPE && self.package.version.major != 1 {
                                     b.kv("DYLIB_COMPATIBILITY_VERSION", self.package.version.major);
                                 }
                                b.kvq("PRODUCT_NAME", &t.target.xcode_product_name);
                                if skip_install_config {
                                    b.kv("SKIP_INSTALL", "YES");
                                 }
                                b.kvq("SUPPORTED_PLATFORMS", &t.target.supported_platforms);
                            })
                            .kv("name", conf_name);
                        });
                    }
                 }
                 o.obj(&conf_release_id, Some("Release"), |c| {
                    c.kv("isa", "XCBuildConfiguration");
                    if let Some((xc_path, xc_id)) = xcconfigs.last() {
                        let xc_filename = xc_path.file_name().unwrap().to_str().unwrap();
                        c.kv("baseConfigurationReference", format_args!("{xc_id} /* {xc_filename} */"));
                    }

                    c.obj("buildSettings", None, |s| {
                        common_build_settings(s, "release");
                    })
                    .kv("name", "Release");
                 })
                 .obj(&conf_debug_id, Some("Debug"), |c| {
                    c.kv("isa", "XCBuildConfiguration");
                    if let Some((xc_path, xc_id)) = xcconfigs.first() {
                        let xc_filename = xc_path.file_name().unwrap().to_str().unwrap();
                        c.kv("baseConfigurationReference", format_args!("{xc_id} /* {xc_filename} */"));
                    }

                    c.obj("buildSettings", None, |s| {
                        common_build_settings(s, "debug");
                    })
                    .kv("name", "Debug");
                 })
                 .section_comment("End XCBuildConfiguration section")
                 .nl()
                 .section_comment("Begin XCConfigurationList section");
                 for t in &targets {
                    o.obj(&t.prod_conf_list_id, Some(&format!("Build configuration list for PBXNativeTarget \"{}\"", t.target.name_label)), |l| {
                        l.kv("isa", "XCConfigurationList")
                        .array("buildConfigurations", None, |c| {
                            c.v(format_args!("{} /* Release */", t.prod_conf_release_id))
                            .v(format_args!("{} /* Debug */", t.prod_conf_debug_id));
                        })
                        .kv("defaultConfigurationIsVisible", "0")
                        .kv("defaultConfigurationName", "Release");
                    });
                 }
                 o.obj(&conf_list_id, Some(&format!("Build configuration list for PBXProject \"{project_name}\"")), |c| {
                    c.kv("isa", "XCConfigurationList")
                    .array("buildConfigurations", None, |c| {
                        c.v(format_args!("{conf_release_id} /* Release */"))
                        .v(format_args!("{conf_debug_id} /* Debug */"));
                    })
                    .kv("defaultConfigurationIsVisible", "0")
                    .kv("defaultConfigurationName", "Release");
                 })
                 .section_comment("End XCConfigurationList section");
            })
            .kv("rootObject", format_args!("{project_id} /* Project object */"));
        let tpl = p.fin();
        Ok(tpl)
    }

    fn relative_path(&self, path: &Path) -> Result<PathBuf, io::Error> {
        let rel_path = pathdiff::diff_paths(fs::canonicalize(path)?, &self.output_dir)
            .ok_or_else(|| {
                io::Error::new(io::ErrorKind::Unsupported, format!("warning: Unable to make relative path from {} to {}", path.display(), self.output_dir.display()))
            })?;

        if rel_path.is_absolute() {
            eprintln!("warning: Unable to make relative path from {} to {}", path.display(), self.output_dir.display());
        }
        Ok(rel_path)
    }

    fn prepare_project_path(&self) -> Result<PathBuf, io::Error> {
        let proj_file_name = format!("{}.xcodeproj", self.custom_project_name.as_ref().unwrap_or(&self.package.name));
        let proj_path = self.output_dir.join(proj_file_name);
        fs::create_dir_all(&proj_path)?;
        Ok(proj_path)
    }

    fn only_allowed_platforms<'a>(&self, supported_platforms: &'a str) -> Cow<'a, str> {
        if let Some(a) = &self.allowed_platforms {
            supported_platforms.split(' ').filter(|&p| a.iter().any(|a| a == p)).collect::<Vec<_>>().join(" ").into()
        } else {
            supported_platforms.into()
        }
    }
}
