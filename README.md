# Rust libaries as Xcode projects

Generates Xcode project files from `Cargo.toml` allowing use of Rust libraries in Mac and iOS applications without leaving Xcode.

## Features

It's better than just launching `cargo build` from a script:

 * Supports Universal Binaries (lipo) and cross-compilation (iOS/tvOS/macOS/watchOS/Simulator/Mac Catalyst, Apple Silicon/Intel).
 * Configures Cargo to follow Xcode's Debug and Release configurations, and uses Xcode's standard build and temporary folders.
 * Makes Xcode aware of dependencies and rebuild Rust code only when needed.
 * Supports Cargo workspaces and multiple targets per crate.
 * Can build dynamic libraries with properly set install name.

The generated Xcode project files are self-contained, and work without `cargo-xcode` dependency. You can distribute the `.xcodeproj` files and use them with just Rust and Xcode.

## Requirements

 * [Rust](https://www.rust-lang.org/), preferably installed via [`rustup`](https://rustup.rs/) (tested with 1.73)
 * [Xcode](https://developer.apple.com/xcode/) (tested with 15.1)
 * Bash

## Installation

```sh
cargo install cargo-xcode
```

## Usage

> TL;DR: Run `cargo xcode` and use the generated project files as **sub**projects in your other Xcode projects.

This tool will generate Rust-aware project files for all binaries and C-compatible libraries in a Cargo workspace. Instead of modifying your Mac/iOS app's project file directly, take advantage of Xcode's ability to nest and combine multiple `xcodeproj` files together. The generated Xcode projects are not meant for standalone use. They are supposed to be used only as **sub**projects of other regular app Xcode projects.

1. If you don't have an existing ObjC/Swift app project yet, create one in Xcode using any Cocoa app template you like. This will be called your "parent project" in later steps.

2. If your Rust project is a library, edit `Cargo.toml` and add:

   ```toml
   [lib]
   crate-type = ["lib", "staticlib"] # cdylib is NOT recommended
   ```

   Only Cargo libraries of type `"staticlib"` or `"cdylib"` are suitable, and `staticlib` is much easier to work with (but keep the `"lib"` crate type for compatibility with other Cargo libraries and tests). If you're not forced to deal with dynamic libraries, don't use them. Use static libraries whenever possible.

3. In the same directory as `Cargo.toml` (or root of a Cargo workspace) run:

   ```sh
   cargo xcode
   ```

   This will generate `<rust-project-name>.xcodeproj`. *Don't* open it yet!

4. Open your parent project (from step 1) in Xcode and add the `<rust-crate-name>.xcodeproj` to the workspace (drag the file into the parent project's sidebar). You should see the dragged Rust-Xcode project embedded in your parent project. If the new Rust-Xcode project appears empty in the sidebar, close all Xcode windows and re-open only the parent project.

5. In your parent project's app's **Build Phases**, in **Link Binary With Libraries** phase, you can now add the Rust libraries from the workspace.

6. There are no automatic bridging headers for Rust. If you need to call Rust functions from C/ObjC/Swift, you will have to write C headers yourself, either by hand, or with help of tools like [cbindgen](https://lib.rs/crates/cbindgen).

[Real-world example of such usage](https://github.com/sindresorhus/Gifski/).

### Advanced usage

You can configure some settings from within Xcode's target's <samp>Build Settings</samp>:

 * `CARGO_XCODE_FEATURES` maps to `--features` flag. `cargo xcode --no-default-features` passes the same flag to `cargo build`.
 * `RUSTUP_TOOLCHAIN` selects which toolchain to use (`nightly`, `1.99.0`, `stable`, empty string for default toolchain). See `rustup toolchain add -h`. `cargo xcode --nightly` picks the nightly.
 * Change <samp>Base SDK</samp> (`SDKROOT`) to iOS, DriverKit, etc. as needed.
 * If you're not cross-compiling, delete the `ADDITIONAL_SDKS` option, or run `cargo xcode --platforms macosx`.

You should almost always use static libraries (`.a`). Dynamic libraries (`.dylib`/`cdylib`) are much harder to build, much harder to embed correctly, and add bloat app bundles from redundant copies of Rust's standard library and dead code. But if you're brave enough to to build a `.dylib` anyway, for including in an application bundle, make sure to set `DYLIB_INSTALL_NAME_BASE` in Xcode's settings to `@executable_path/../Frameworks/` or whatever location you're going to copy the library to.

If you're putting a Rust a binary inside an app bundle, you may need to set `SKIP_INSTALL=YES` or run `cargo xcode --skip-install`.

Xcode supports reading config from text files, which is useful for sharing settings across multiple projects. You can specify these with `--xcconfig debug.xcconfig --xcconfig release.xcconfig`.

## Limitations

Rust binaries are exported as command-line tools. This tool intentionally does not make app bundles. If you want to build a Mac GUI app, create one as ObjC or Swift project in Xcode and call Rust code via a Rust static library, or use [Tauri](https://lib.rs/crates/tauri).

## DIY-Tier platforms

AppleTV, Mac Catalyst, VisionOS, and some other rare platforms don't have pre-built rustup targets. `cargo xcode` will try to use nightly toolchain and `-Zbuild-std` feature to support these platforms. In case the auto-detection doesn't work:

1. Set `RUSTUP_TOOLCHAIN` build setting to `nightly` (or specific nightly version if you require).
2. In a target › <samp>Build Phases</samp> tab › <samp>Compile Sources</samp> phase, edit the <samp>Compiler Flags</samp> column of `Cargo.toml`, and append `-Zbuild-std` there.
